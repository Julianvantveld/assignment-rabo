//
//  CSVHelper.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 03/09/2021.
//

import Foundation

class CSVHelper {
    
    public static let sharedInstance = CSVHelper()
    
    let workQueue: DispatchQoS.QoSClass = .utility
    var workDispatchQueue: DispatchQueue {
        return DispatchQueue.global(qos: workQueue)
    }
    
    /// Change the state of an upload message
    /// - Parameters:
    ///   - fileName: the file name as string of the CSV file
    ///   - completion: data parsed from CSV as asynchronous completion
    func getCSVData(fileName: String, completion: @escaping ([[String]]?) -> Void) {
        
        // First we get the filepath
        guard let filepath = Bundle.main.path(forResource: fileName, ofType: "csv") else {
            return completion(nil)
        }
        
        // Async excecution outside of main thread
        workDispatchQueue.async { [weak self] in
            
            // Check for strong reference of self
            guard self != nil else { return }
            
            // Create reader, if fails it means file cannot be opened
            guard let reader = LineReader(path: filepath) else {
                return completion(nil)
            }
            
            var data: [[String]] = []
            
            // Prepare data from file
            for line in reader {
                
                // Release memory for each loop to prevent memory overflow
                autoreleasepool {
                    
                    // Create weak reference to strong type
                    let mutableLine = line
                    
                    // Parse line to remove breaklines and create an array of rows (commaseperated)
                    let row = mutableLine
                        .replacingOccurrences(of: "\r\n", with: "")
                        .components(separatedBy: ",")
                        .map({ $0.replacingOccurrences(of: "\"", with: "") })
                    
                    data.append(row)
                }
            }
            
            // return data if composing array is complete
            completion(data)
        }
    }
}
