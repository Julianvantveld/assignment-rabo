//
//  FontHelper.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 05/09/2021.
//

import Foundation
import UIKit

extension UIFont {
    
    enum RaboFont: String {
        case light = "MyriadPro-Light"
        case semiBold = "MyriadPro-Semibold"
        case semiBoldItalic = "MyriadPro-SemiboldIt"
        case regular = "MyriadPro-Regular"
        case bold = "MyriadPro-Bold"
        case boldItalic = "MyriadPro-BoldIt"
        case condensed = "MyriadPro-Cond"
        case condensedItalic = "MyriadPro-CondIt"
        case boldCondensed = "MyriadPro-BoldCond"
        case boldCondensedItalic = "MyriadPro-BoldCondIt"
    }
    
    enum RaboFontSize: CGFloat {
        case small = 12.0
        case subtitle = 14.0
        case regular = 16.0
        case title = 18.0
        case header = 24.0
        case large = 34.0
    }
    
    static func rabo(_ name: RaboFont = .regular, size: RaboFontSize) -> UIFont {
        return UIFont(name: name.rawValue, size: size.rawValue) ?? UIFont.systemFont(ofSize: size.rawValue)
    }
}
