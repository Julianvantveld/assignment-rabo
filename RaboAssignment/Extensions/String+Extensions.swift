//
//  String+Extensions.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 03/09/2021.
//

import Foundation

extension String {
    
    func stringToDate(format: String) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        dateFormatter.locale = Locale(identifier: Locale.current.identifier)
        
        if dateFormatter.date(from: self) == nil {
            return nil
        }
        return dateFormatter.date(from: self)
    }
}
