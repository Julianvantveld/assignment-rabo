//
//  Date+Extensions.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 07/09/2021.
//

import Foundation

extension Date {
    
    func dateToString(format: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: self)
    }
}
