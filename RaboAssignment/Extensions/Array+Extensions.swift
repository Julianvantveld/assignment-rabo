//
//  Array+Extensions.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 05/09/2021.
//

import Foundation

extension Array {
    
    /// Chop up array into multiple sub parts
    /// - Parameters:
    ///   - size: Size of chunks in Array
    func chunked(into size: Int) -> [[Element]] {
        return stride(from: 0, to: count, by: size).map {
            Array(self[$0 ..< Swift.min($0 + size, count)])
        }
    }
}
