//
//  CSVModel.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 03/09/2021.
//

import Foundation

struct IssueModel {
    
    var firstName: String?
    var lastName: String?
    var issueCount: Int?
    var dateOfBirth: Date?
}
