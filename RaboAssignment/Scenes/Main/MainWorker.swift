//
//  MainWorker.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 02/09/2021.
//

import UIKit

protocol MainWorkerLogic: AnyObject {
    
    func parseCSVData(_ file: String, _ data: [[String]], completion: @escaping (String, [IssueModel]) -> Void)
}

class MainWorker: MainWorkerLogic {
    
    /// Parse the CSV Data into workable data models
    /// - Parameters:
    ///   - data: The raw data reveived from CSV
    ///   - completion: Returning data models which confirm to expectation
    func parseCSVData(_ file: String, _ data: [[String]], completion: @escaping (String, [IssueModel]) -> Void) {
        var data = data
        
        // We get the amount of columns in the CSV based on first "header" line
        let numberOfColumns = data.first?.count ?? 0
        
        // Remove header row from data
        data.removeFirst()
        
        // Chunk data in bite-sized bits for view updating to prevent user from waiting for large data sets
        let chunks = data.chunked(into: 5000)
        
        DispatchQueue.global(qos: .utility).async {
            for chunk in chunks {
                
                var issueList: [IssueModel] = []
                
                autoreleasepool {
                    
                    for item in chunk where item.count == numberOfColumns {
                        
                        // Foreach chunk we created, start by creating an IssueModel to handle in the presenter
                        autoreleasepool {
                            
                            let weak = item
                            
                            let firstName = weak[0]
                            let surname = weak[1]
                            let issueCount = Int(weak[2])
                            let dateOfBirth = (weak[3]).stringToDate(format: "yyyy-MM-dd'T'HH:mm:ss")
                            
                            let issue = IssueModel(firstName: firstName, lastName: surname, issueCount: issueCount, dateOfBirth: dateOfBirth)
                            
                            issueList.append(issue)
                        }
                    }
                }
                
                completion(file, issueList)
            }
        }
    }
}
