//
//  MainRouter.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 02/09/2021.
//

import UIKit

@objc protocol MainRoutingLogic {
    
}

protocol MainDataPassing {

    var dataStore: MainDataStore? { get }
}

class MainRouter: MainRoutingLogic, MainDataPassing {

    weak var viewController: MainViewController?
    var dataStore: MainDataStore?

    // MARK: Routing
    
    // Empty implementation for Assignment purposes

//    func routeToSomewhere(segue: UIStoryboardSegue?) {
//
//        let destinationVC = SomewhereViewController()
//        if let sourceDS = dataStore, var destinationDS = destinationVC.router?.dataStore {
//
//            passData(from: sourceDS, to: &destinationDS)
//        }
//
//        if let sourceVC = viewController {
//            navigate(from: sourceVC, to: destinationVC)
//        }
//    }
//
//    // MARK: Navigation
//
//    func navigate(from source: MainViewController, to destination: SomewhereViewController) {
//
//        source.show(destination, sender: nil)
//    }
//
//    // MARK: Passing data
//
//    func passData(from source: MainDataStore, to destination: inout SomewhereDataStore) {
//
//        destination.name = source.name
//    }
}
