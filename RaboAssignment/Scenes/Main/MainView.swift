//
//  MainView.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 02/09/2021.
//

import UIKit

private struct ViewTraits {
    static let leadingMargin: CGFloat = 16
    static let trailingMargin: CGFloat = 16
    static let buttonHeight: CGFloat = 56
    static let buttonMarginTop: CGFloat = 24
    static let buttonMarginBottom: CGFloat = -16
}

class MainView: BaseView {

    // MARK: Public
    
    let tableView: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundColor = .clear
        tableView.separatorStyle = .none
        tableView.showsHorizontalScrollIndicator = false
        tableView.backgroundView = EmptyTableView(frame: tableView.frame)
        return tableView
    }()
    
    let fetchButton: RaboButton = {
       let button = RaboButton()
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle(R.string.localizable.home_button_title(), for: .normal)
        button.disabledColor = .gray
        return button
    }()

    override func setupViews() {
        
        super.setupViews()
        backgroundColor = .systemBackground

    }
    
    override func setupViewHierarchy() {
        
        super.setupViewHierarchy()
        
        addSubview(tableView)
        addSubview(fetchButton)
    }
    
    override func setupViewConstraints() {
        
        super.setupViewConstraints()
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
            tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
            tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
            tableView.bottomAnchor.constraint(equalTo: fetchButton.topAnchor, constant: -ViewTraits.buttonMarginTop),
            
            fetchButton.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor, constant: ViewTraits.leadingMargin),
            fetchButton.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor, constant: -ViewTraits.trailingMargin),
            fetchButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: ViewTraits.buttonMarginBottom),
            fetchButton.heightAnchor.constraint(equalToConstant: ViewTraits.buttonHeight)
        ])
    }
}
