//
//  MainModels.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 02/09/2021.
//

import UIKit

enum Main {

    // MARK: Use cases
    enum Display {

        struct Request {
        }

        struct Response {
        }

        struct ViewModel {
            let viewTitle: String
            let fileNames: [String]
        }
    }
    
    enum Content {

        struct Request {
            let fileName: String
        }

        struct Response {
            let data: [IssueModel]
        }

        struct ViewModel {
            let content: [ContentDisplayModel]
        }
        
        struct ContentDisplayModel {
            var name: String = ""
            var issueCount: String = ""
            var dateOfBirth: String = ""
        }
    }
}
