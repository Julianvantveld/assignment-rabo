//
//  MainInteractor.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 02/09/2021.
//

import UIKit

protocol MainBusinessLogic {
    
    func handleDisplayRequest(request: Main.Display.Request)
    func handleFetchContentRequest(request: Main.Content.Request)
}

protocol MainDataStore {
    
    var csvData: [IssueModel] { get set }
    var fileInProgress: String? { get set }
}

class MainInteractor: MainBusinessLogic, MainDataStore {
    
    var presenter: MainPresentationLogic?
    var worker: MainWorker = MainWorker()
    var csvData: [IssueModel] = []
    var fileInProgress: String?
    
    func handleDisplayRequest(request: Main.Display.Request) {
        
        let response = Main.Display.Response()
        presenter?.presentView(response: response)
    }
    
    func handleFetchContentRequest(request: Main.Content.Request) {
        
        fileInProgress = request.fileName
        
        // On new Fetch request, delete all previous data
        self.csvData.removeAll()
        
        CSVHelper.sharedInstance.getCSVData(fileName: request.fileName) { issueList in
            
            if let list = issueList {
                
                // Send data to worker to parse into workable data
                self.worker.parseCSVData(request.fileName, list) { file, chunk in
                    
                    if file == self.fileInProgress {
                        self.csvData.append(contentsOf: chunk)
                        
                        // Chunks of data will come in, send it to Presenter to format chunks into display data
                        let response = Main.Content.Response(data: chunk)
                        self.presenter?.presentFetchContent(response: response)
                    }
                }
            }
        }
    }
}
