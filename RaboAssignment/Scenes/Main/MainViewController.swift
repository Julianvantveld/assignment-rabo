//
//  MainViewController.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 02/09/2021.
//

import UIKit

protocol MainDisplayLogic: AnyObject {

    func displayView(viewModel: Main.Display.ViewModel)
    func displayFetchContent(viewModel: Main.Content.ViewModel)
}

class MainViewController: UIViewController {

    var interactor: MainBusinessLogic?
    var router: (MainRoutingLogic & MainDataPassing)?

    private let sceneView = MainView()
    var contentArray: [Main.Content.ContentDisplayModel] = []
    var fileNames: [String] = []

    // MARK: Object lifecycle
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {

        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)
        setup()
    }

    // MARK: Setup
    private func setup() {

        let viewController = self
        let interactor = MainInteractor()
        let presenter = MainPresenter()
        let router = MainRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
        router.dataStore = interactor
    }

    // MARK: View lifecycle
    override func loadView() {
        view = sceneView
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.largeTitleTextAttributes = [.font: UIFont.rabo(.boldItalic, size: .large), .foregroundColor: R.color.rb_blue() ?? .white]
        
        sceneView.tableView.dataSource = self
        sceneView.tableView.delegate = self
        sceneView.tableView.register(IssueTableViewCell.self, forCellReuseIdentifier: "IssueTableViewCell")
        
        sceneView.fetchButton.addTarget(self, action: #selector(fetchContent), for: .touchUpInside)
        
        handleDisplayRequest()
    }
    
    @objc func fetchContent() {
        
        let alert = UIAlertController(title: R.string.localizable.file_select(), message: nil, preferredStyle: .actionSheet)
        
        for item in fileNames {
            let action = UIAlertAction(title: item, style: .default) { _ in

                self.fetchFileContent(with: item)
            }
            
            alert.addAction(action)
        }
        
        let cancelAction = UIAlertAction(title: R.string.localizable.cancel(), style: .cancel) { _ in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true)
    }
    
    func fetchFileContent(with fileName: String) {
        contentArray.removeAll()
        sceneView.tableView.reloadData()
        sceneView.tableView.separatorStyle = contentArray.isEmpty ? .none : .singleLine
        sceneView.fetchButton.loadingSpinner.startAnimating()
        sceneView.fetchButton.isEnabled = false
        
        let request = Main.Content.Request(fileName: fileName)
        interactor?.handleFetchContentRequest(request: request)
    }
}

// MARK: Output --- Request
extension MainViewController {

    func handleDisplayRequest() {
        
        let request = Main.Display.Request()
        interactor?.handleDisplayRequest(request: request)
    }
}

// MARK: Input --- Display
extension MainViewController: MainDisplayLogic {

    func displayView(viewModel: Main.Display.ViewModel) {

        self.title = viewModel.viewTitle
        self.fileNames = viewModel.fileNames
    }
    
    func displayFetchContent(viewModel: Main.Content.ViewModel) {
        
        self.contentArray.append(contentsOf: viewModel.content)
        
        sceneView.tableView.backgroundView = contentArray.isEmpty ? EmptyTableView(frame: sceneView.tableView.frame) : UIView()
        sceneView.tableView.separatorStyle = contentArray.isEmpty ? .none : .singleLine
        
        sceneView.tableView.reloadData()
        sceneView.fetchButton.loadingSpinner.stopAnimating()
        sceneView.fetchButton.isEnabled = true
    }
}

// MARK: Routing --- Navigate next scene
extension MainViewController {

    private func prepareForNextScene() {
        // Empty implementation for assignment purposes
    }
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "IssueTableViewCell") as? IssueTableViewCell {
            let data = contentArray[indexPath.row]
            cell.setData(name: data.name, date: data.dateOfBirth, issueCount: data.issueCount)
            return cell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
