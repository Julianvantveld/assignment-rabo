//
//  MainPresenter.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 02/09/2021.
//

import UIKit

protocol MainPresentationLogic {

    func presentView(response: Main.Display.Response)
    func presentFetchContent(response: Main.Content.Response)
}

class MainPresenter: MainPresentationLogic {

    weak var viewController: MainDisplayLogic?

    func presentView(response: Main.Display.Response) {
        
        let viewModel = Main.Display.ViewModel(
            viewTitle: R.string.localizable.home_view_title(),
            fileNames: ["issues_default", "issues_18MB", "issues_malformed"]
        )
        
        viewController?.displayView(viewModel: viewModel)
    }
    
    func presentFetchContent(response: Main.Content.Response) {
        
        // Received data is raw and contains "model data" not nesseccary for display.
        // In the presenter we parse the raw data into exactly that which the viewController
        // will display. For instance a combination of first- and last name instead of two values
        
        let data = response.data.sorted(by: { $0.issueCount ?? 0 > $1.issueCount ?? 0 }).map({ item -> Main.Content.ContentDisplayModel in
            
            var model = Main.Content.ContentDisplayModel()
            
            if (item.firstName ?? "").isEmpty && (item.lastName ?? "").isEmpty {
                model.name = R.string.localizable.unknown_data()
            } else {
                model.name = R.string.localizable.name("\(item.firstName?.capitalized ?? "") \(item.lastName?.capitalized ?? "")")
            }
            
            model.issueCount = R.string.localizable.number(item.issueCount?.description ?? "?")
            model.dateOfBirth = R.string.localizable.birth_date(item.dateOfBirth?.dateToString(format: "dd-MM-yyyy") ?? R.string.localizable.unknown_data())
            return model
        })
        
        // Everything was done asynchronousely. Now we want to update the UI so need to go back to main thread.
        DispatchQueue.main.async {
            let viewModel = Main.Content.ViewModel(content: data)
            self.viewController?.displayFetchContent(viewModel: viewModel)
        }
    }
}
