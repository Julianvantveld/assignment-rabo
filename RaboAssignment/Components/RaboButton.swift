//
//  RaboButton.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 06/09/2021.
//

import Foundation
import UIKit

private struct ViewTraits {
    static let defaultShadowOpacity: Float = 0.3
    static let defaultShadowOffset: CGFloat = 2
    static let defaultShadowRadius: CGFloat = 3
    static let leadingPadding: CGFloat = 12
}

class RaboButton: UIButton {

    private var shadowLayer: CAShapeLayer?
    private var defaultButtonColor = R.color.rb_orange() ?? .black
    private var defaultTitleColor = UIColor.white
    private var disabledButtonColor = UIColor.lightGray
    private var defaultBorderColor = UIColor.clear
    private var boundInset = UIEdgeInsets.zero

    @IBInspectable var buttonColor: UIColor {
        get {
            return defaultButtonColor
        }
        set {
            defaultButtonColor = newValue
        }
    }

    @IBInspectable var disabledColor: UIColor {
        get {
            return disabledButtonColor
        }
        set {
            disabledButtonColor = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        get {
            return defaultBorderColor
        }
        set {
            defaultBorderColor = newValue
        }
    }

    var inset: UIEdgeInsets {
        get {
            return boundInset
        }
        set {
            boundInset = newValue
        }
    }
    
    var loadingSpinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView()
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.color = .white
        spinner.hidesWhenStopped = true
        return spinner
    }()
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 8
    }
    
    func setup() {
        clipsToBounds = true
        setTitleColor(defaultTitleColor, for: .normal)
        setBackgroundColor(defaultButtonColor, forState: .normal)
        setBackgroundColor(.gray, forState: .highlighted)
        
        addSubview(loadingSpinner)
        
        addCustomConstraints()
    }
    
    func addCustomConstraints() {
        
        NSLayoutConstraint.activate([
            loadingSpinner.leadingAnchor.constraint(equalTo: leadingAnchor, constant: ViewTraits.leadingPadding),
            loadingSpinner.centerYAnchor.constraint(equalTo: centerYAnchor)
        ])
    }
}
