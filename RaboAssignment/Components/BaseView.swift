//
//  BaseView.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 02/09/2021.
//

import Foundation
import UIKit

class BaseView: UIView {
    
    override init(frame: CGRect) {
        
        // Init
        
        super.init(frame: frame)
        
        setupViews()
        setupViewHierarchy()
        setupViewConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)

        setupViews()
        setupViewHierarchy()
        setupViewConstraints()
    }
    
    func setupViews() {
        // Base view doesn't need to do any setup
    }
    
    func setupViewHierarchy() {
        // Base view doesn't need to do any setup
    }
    
    func setupViewConstraints() {
        // Base view doesn't need to do any setup
    }
}
