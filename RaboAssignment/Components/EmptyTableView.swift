//
//  EmptyTableView.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 05/09/2021.
//

import Foundation
import UIKit

private struct ViewTraits {
    static let iconWidth: CGFloat = 250
    static let iconHeight: CGFloat = 150
    static let padding: CGFloat = 16
    static let bottomMargin: CGFloat = 24
}

class EmptyTableView: BaseView {
    
    // MARK: Public
    
    let iconView: UIImageView = {
        let iconView = UIImageView()
        iconView.translatesAutoresizingMaskIntoConstraints = false
        iconView.image = R.image.rb_empty_state()
        iconView.contentMode = .scaleAspectFit
        return iconView
    }()
    
    let emptyLabel: UILabel = {
        let emptyLabel = UILabel()
        emptyLabel.translatesAutoresizingMaskIntoConstraints = false
        emptyLabel.text = R.string.localizable.empty_view_title()
        emptyLabel.font = UIFont.rabo(.boldItalic, size: .header)
        emptyLabel.textColor = R.color.rb_blue()
        emptyLabel.numberOfLines = 0
        emptyLabel.textAlignment = .center
        return emptyLabel
    }()
    
    override func setupViews() {
        
        super.setupViews()
        backgroundColor = .systemBackground
        
    }
    
    override func setupViewHierarchy() {
        
        super.setupViewHierarchy()
        
        addSubview(iconView)
        addSubview(emptyLabel)
    }
    
    override func setupViewConstraints() {
        
        super.setupViewConstraints()
        
        NSLayoutConstraint.activate([
            iconView.bottomAnchor.constraint(equalTo: centerYAnchor),
            iconView.centerXAnchor.constraint(equalTo: centerXAnchor),
            iconView.widthAnchor.constraint(equalToConstant: ViewTraits.iconWidth),
            iconView.heightAnchor.constraint(equalToConstant: ViewTraits.iconHeight),
            
            emptyLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: ViewTraits.padding),
            emptyLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: ViewTraits.padding),
            emptyLabel.topAnchor.constraint(equalTo: iconView.bottomAnchor, constant: ViewTraits.bottomMargin)
        ])
    }
}
