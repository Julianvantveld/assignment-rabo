//
//  IssueTableViewCell.swift
//  RaboAssignment
//
//  Created by Julian van 't Veld on 07/09/2021.
//

import Foundation
import UIKit

private struct ViewTraits {
    static let verticalPadding: CGFloat = 8
    static let horizontalPadding: CGFloat = 16
    static let verticalSpacing: CGFloat = 12
}

class IssueTableViewCell: UITableViewCell {
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.rabo(.semiBold, size: .regular)
        label.textColor = R.color.rb_dark_text()
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.rabo(.semiBold, size: .regular)
        label.textColor = R.color.rb_dark_text()
        return label
    }()
    
    let issueCountLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.rabo(.bold, size: .header)
        label.textColor = R.color.rb_dark_text()
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        selectionStyle = .none
        
        // Add subviews
        contentView.addSubview(nameLabel)
        contentView.addSubview(dateLabel)
        contentView.addSubview(issueCountLabel)
        
        addCustomConstraints()
        
        accessibilityTraits = .staticText
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        fatalError("init(coder:) has not been implemented")
    }
    
    func addCustomConstraints() {
        
        NSLayoutConstraint.activate([

            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: ViewTraits.verticalPadding),
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: ViewTraits.horizontalPadding),
            
            dateLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: ViewTraits.verticalSpacing),
            dateLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -ViewTraits.verticalPadding),
            dateLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: ViewTraits.horizontalPadding),
            
            issueCountLabel.topAnchor.constraint(equalTo: contentView.topAnchor),
            issueCountLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            issueCountLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -ViewTraits.horizontalPadding)
        ])
    }
    
    func setData(name: String, date: String, issueCount: String) {
        nameLabel.text = name
        dateLabel.text = date
        issueCountLabel.text = issueCount
    }
}
