# Assignment Rabobank Team Native

This app is built with the goal to parse CSV files and show the contents on screen, for an assignment for Team Native for Rabobank. Below are screenshots attached of the application.
The main branch contains the final release of the assignment.

## Screenshots
<p align="center">
    <img src="Example/Screenshots.png" alt="Screenshots">
</p>

## Setup App
- Open a terminal in `assignment-rabo`
- Run `Pod install`
- Open the project file `RaboAssignment.xcworkspace`
- Run the app

## Future development
What would I improve if I had more time to spend on the assignment?

First of all I would focus on accepting different types of CSV. At the moment only CSV files with four columns will be parsed as expected. Malformed content is parsed up to a certain extent but there are some improvements to be made to make it "fool-proof".

The document picker at the moment is based on static files included in the project. This was done intentionally to give a variety of files with different use cases for review of the assessment. In the future an actual document selector can be implemented to give the user access to files on their phone and upload these to get parsed.
