//
//  MainSpies.swift
//  RaboAssignmentTests
//
//  Created by Julian van 't Veld on 02/09/2021.
//

@testable import RaboAssignment
import XCTest

class MainPresentationLogicSpy: MainPresentationLogic {

    var presentViewCalled = false
    var presentViewResponse: Main.Display.Response?
    
    var presentFetchContentCalled = false
    var presentFetchContentResponse: Main.Content.Response?
    
    func presentView(response: Main.Display.Response) {
        
        presentViewCalled = true
        presentViewResponse = response
    }
    
    func presentFetchContent(response: Main.Content.Response) {
        
        presentFetchContentCalled = true
        presentFetchContentResponse = response
    }
}

class MainDisplayLogicSpy: MainDisplayLogic {
    
    var displayViewCalled = false
    var displayViewViewModel: Main.Display.ViewModel?
    
    var displayContentCalled = false
    var displayContentViewModel: Main.Content.ViewModel?
    
    func displayView(viewModel: Main.Display.ViewModel) {
        
        displayViewCalled = true
        displayViewViewModel = viewModel
    }
    
    func displayFetchContent(viewModel: Main.Content.ViewModel) {
        
        displayContentCalled = true
        displayContentViewModel = viewModel
    }
    
}

class MainBusinessLogicSpy: MainBusinessLogic {
    
    var handleDisplayCalled = false
    var handleDisplayRequest: Main.Display.Request?
    
    var handleFetchContentRequstCalled = false
    var handleFetchContentRequest: Main.Content.Request?
    
    func handleDisplayRequest(request: Main.Display.Request) {
        
        handleDisplayCalled = true
        handleDisplayRequest = request
    }
    
    func handleFetchContentRequest(request: Main.Content.Request) {
        
        handleFetchContentRequstCalled = true
        handleFetchContentRequest = request
    }
}
