//
//  MainPresenterTests.swift
//  RaboAssignmentTests
//
//  Created by Julian van 't Veld on 02/09/2021.
//

@testable import RaboAssignment
import XCTest
import Nimble

class MainPresenterTests: XCTestCase {
    // MARK: Subject under test
    
    var sut: MainPresenter?
        
    // MARK: Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupMainPresenter()
    }
    
    override func tearDown() {

        super.tearDown()
    }

    // MARK: Test setup
    func setupMainPresenter() {

        let viewController = MainViewController()
        
        let presenter = MainPresenter()
        
        sut = presenter
        sut?.viewController = viewController
    }
    
    func testDisplayView() {
        
        // Given
        let viewControllerSpy = MainDisplayLogicSpy()
        sut?.viewController = viewControllerSpy
        
        let response = Main.Display.Response()
        let includedFiles = ["issues_default", "issues_18MB", "issues_malformed"]
              
        // When
        self.sut?.presentView(response: response)
        
        // Then
        expect(viewControllerSpy.displayViewCalled)
            .to(beTrue(), description: "Method should be called")
        expect(viewControllerSpy.displayViewViewModel?.viewTitle)
            .to(equal(R.string.localizable.home_view_title()), description: "Value must match")
        expect(viewControllerSpy.displayViewViewModel?.fileNames.count)
            .to(equal(includedFiles.count), description: "Files should be present")
    }
    
    func testdisplayFetchContent() {
        
        // Given
        let viewControllerSpy = MainDisplayLogicSpy()
        sut?.viewController = viewControllerSpy
        
        let models: [IssueModel] = [
            IssueModel(firstName: "Henk", lastName: "Klaassen", issueCount: 2, dateOfBirth: Date()),
            IssueModel(firstName: "Jan-Ties", lastName: "Roderik", issueCount: 1, dateOfBirth: nil)
        ]
        
        let response = Main.Content.Response(data: models)
              
        // When
        self.sut?.presentFetchContent(response: response)
        
        // Then
        expect(viewControllerSpy.displayContentCalled)
            .toEventually(beTrue(), description: "Method should be called")
        expect(viewControllerSpy.displayContentViewModel?.content[0].name)
            .to(equal(R.string.localizable.name("Henk Klaassen")), description: "Value should match")
        expect(viewControllerSpy.displayContentViewModel?.content[0].dateOfBirth)
            .to(equal(R.string.localizable.birth_date(Date().dateToString(format: "dd-MM-yyyy"))), description: "Value should match")
        expect(viewControllerSpy.displayContentViewModel?.content[1].dateOfBirth)
            .to(equal(R.string.localizable.birth_date(R.string.localizable.unknown_data())), description: "Value should match")
        
    }
    
    func testdisplayFetchContentEmpty() {
        
        // Given
        let viewControllerSpy = MainDisplayLogicSpy()
        sut?.viewController = viewControllerSpy
        
        let models: [IssueModel] = []
        
        let response = Main.Content.Response(data: models)
              
        // When
        self.sut?.presentFetchContent(response: response)
        
        // Then
        expect(viewControllerSpy.displayContentCalled)
            .toEventually(beTrue(), description: "Method should be called")
    }
}
