//
//  MainViewControllerTests.swift
//  RaboAssignmentTests
//
//  Created by Julian van 't Veld on 02/09/2021.
//

@testable import RaboAssignment
import XCTest
import Nimble

class MainViewControllerTests: XCTestCase {
    // MARK: Subject under test
    
    var sut: MainViewController?
        
    // MARK: Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupMainViewController()
    }
    
    override func tearDown() {

        super.tearDown()
    }

    // MARK: Test setup
    func setupMainViewController() {

        let interactor = MainInteractor()
        
        let viewController = MainViewController()
        
        sut = viewController
        sut?.interactor = interactor
    }
    
    func testDisplayView() {
        
        // Given
        let businessLogicSpy = MainBusinessLogicSpy()
        sut?.interactor = businessLogicSpy
              
        // When
        self.sut?.handleDisplayRequest()
        
        // Then
        expect(businessLogicSpy.handleDisplayCalled)
            .to(beTrue(), description: "Method should be called")
    }
    
    func testFetchFileContent() {
        
        // Given
        let businessLogicSpy = MainBusinessLogicSpy()
        sut?.interactor = businessLogicSpy
              
        // When
        self.sut?.fetchFileContent(with: "issues_default")
        
        // Then
        expect(businessLogicSpy.handleFetchContentRequstCalled)
            .to(beTrue(), description: "Method should be called")
    }
}
