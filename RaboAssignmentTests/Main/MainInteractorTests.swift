//
//  RaboAssignmentTests.swift
//  RaboAssignmentTests
//
//  Created by Julian van 't Veld on 02/09/2021.
//

@testable import RaboAssignment
import XCTest
import Nimble

class MainInteractorTests: XCTestCase {
    // MARK: Subject under test
    
    var sut: MainInteractor?
        
    // MARK: Test lifecycle
    
    override func setUp() {
        super.setUp()
        setupMainInteractor()
    }
    
    override func tearDown() {

        super.tearDown()
    }

    // MARK: Test setup
    func setupMainInteractor() {

        let presenter = MainPresenter()
        
        let interactor = MainInteractor()
        
        sut = interactor
        sut?.presenter = presenter
    }
    
    func testPresentView() {
        
        // Given
        let presentationSpy = MainPresentationLogicSpy()
        sut?.presenter = presentationSpy
        
        let request = Main.Display.Request()
              
        // When
        self.sut?.handleDisplayRequest(request: request)
        
        // Then
        expect(presentationSpy.presentViewCalled)
            .to(beTrue(), description: "Method should be called")
    }
    
    func testPresentContent() {
        
        // Given
        let presentationSpy = MainPresentationLogicSpy()
        sut?.presenter = presentationSpy
        
        let request = Main.Content.Request(fileName: "issues_default")
              
        // When
        self.sut?.handleFetchContentRequest(request: request)
        
        // Then
        expect(presentationSpy.presentFetchContentCalled)
            .toEventually(beTrue(), description: "Method should be called")
        expect(presentationSpy.presentFetchContentResponse?.data)
            .toNot(beNil(), description: "Data should be present")
    }
    
    func testPresentMalformedContent() {
        
        // Given
        let presentationSpy = MainPresentationLogicSpy()
        sut?.presenter = presentationSpy
        
        let request = Main.Content.Request(fileName: "issues_malformed")
              
        // When
        self.sut?.handleFetchContentRequest(request: request)
        
        // Then
        expect(presentationSpy.presentFetchContentCalled)
            .toEventually(beTrue(), description: "Method should be called")
        expect(presentationSpy.presentFetchContentResponse?.data)
            .toNot(beNil(), description: "Data should be present")
    }
}
